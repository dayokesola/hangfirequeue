﻿
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace HashTalk.Web.Queue.Controllers
{
    [RoutePrefix("api/HangFire")]
    public class HangFireController : ApiController
    {
        /// <summary>
        /// HAngfire queuer
        /// </summary>
        /// <param name="url"></param>
        /// <param name="scheduleDate"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("BackgroundEnqueue")]
        public IHttpActionResult BackgroundEnqueue(string url, string scheduleDate = "")
        { 
            var jobId = string.Empty;
            if (string.IsNullOrEmpty(scheduleDate))
            {
               jobId = BackgroundJob.Enqueue(() => HangFireUrl.Call(url));
            }
            else
            { 
                var offset = new DateTimeOffset(Convert.ToDateTime(scheduleDate));
                jobId = BackgroundJob.Schedule(() => HangFireUrl.Call(url), offset);
            }
            return Ok(jobId);
        }


        [HttpGet]
        [Route("Recurring")]
        public IHttpActionResult Recurring(string url, string cronExpression, string jobName = null)
        {
            if (string.IsNullOrEmpty(jobName))
            {
                jobName = url;
            }
            //Add recurring job
            RecurringJob.AddOrUpdate(jobName, () => HangFireUrl.Call(url), cronExpression);
            return Ok(string.Format("Recurring job {0}  with Url {1} scheduled successfully", jobName, url));
        }
    }

    public class HangFireUrl
    {
        public static string Call(string url)
        {
            var resp = "";
            string WEBSERVICE_URL = url;
            try
            {
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    webRequest.Method = "GET"; 
                    webRequest.ContentType = "application/json";
                    webRequest.Timeout = 90000;
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            resp = sr.ReadToEnd();
                            //Console.WriteLine(String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                resp = ex.Message;
            }

            return resp;


            //var uri = new Uri(url);
            //var path = uri.Scheme + "://" + uri.Authority;
            //var client = new HttpClient
            //{
            //    BaseAddress = new Uri(path)
            //};
            //HttpResponseMessage responseMessage = null;
            //responseMessage = client.GetAsync(uri.PathAndQuery).Result;
            //var data = responseMessage.Content.ReadAsAsync<string>().Result;
            //return data; 
        }
    }
}
