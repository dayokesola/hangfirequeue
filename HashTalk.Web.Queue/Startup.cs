﻿
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.MySql; 
using HashTalk.Web.Queue.Filter;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Transactions;
using System.Web.Http;

[assembly: OwinStartup(typeof(HashTalk.Web.Queue.Startup))]

namespace HashTalk.Web.Queue
{
    public class Startup
    {

        private IEnumerable<IDisposable> GetHangfireServers()
        {

            Hangfire.GlobalConfiguration.Configuration.UseStorage(
                new MySqlStorage(ConfigurationManager.ConnectionStrings["hashtalk_queue"].ConnectionString,
                new MySqlStorageOptions
                {
                    TransactionIsolationLevel = IsolationLevel.ReadCommitted,
                    QueuePollInterval = TimeSpan.FromSeconds(15),
                    JobExpirationCheckInterval = TimeSpan.FromHours(1),
                    CountersAggregateInterval = TimeSpan.FromMinutes(5),
                    PrepareSchemaIfNecessary = true,
                    DashboardJobListLimit = 50000,
                    TransactionTimeout = TimeSpan.FromMinutes(1),

                }));


            yield return new BackgroundJobServer(new BackgroundJobServerOptions
            {
                WorkerCount = 1
            });
        }


        public static HttpConfiguration HttpConfiguration { get; private set; }
        public void Configuration(IAppBuilder app)
        { 
            HttpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(HttpConfiguration);
             

            app.UseHangfireAspNet(GetHangfireServers);
            //app.UseHangfireDashboard("/hangfire");

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                //Authorization = new[] { new MyAuthorizationFilter() } 
                Authorization = new[] {
                new BasicAuthAuthorizationFilter(
                new BasicAuthAuthorizationFilterOptions
                {
                    // Require secure connection for dashboard
                    RequireSsl = false,
                    // Case sensitive login checking
                    LoginCaseSensitive = true,
                    SslRedirect = false,
                    // Users
                    Users = new[]
                    {
                        new BasicAuthAuthorizationUser
                        {
                            Login = "hangadmin",
                            // Password as plain text, SHA1 will be used
                            PasswordClear = "qwerty123"
                        }
                    }
                })
                }

            }); ;
            app.UseWebApi(HttpConfiguration);
        }
    }
}
